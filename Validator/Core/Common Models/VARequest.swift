//
//  VARequest.swift
//  ValidatorExample
//
//  Created by Orhan DALGARA on 9.04.2020.
//  Copyright © 2020 Orhan DALGARA. All rights reserved.
//

import UIKit


class VASampleRequest:VARequest<VAValidateReceiptResponse> {
    
    
}

public enum VAHttpMethod: String {
    case GET = "GET"
    case POST = "POST"
    case PUT = "PUT"
    case DELETE = "DELETE"
}


typealias VARequestFailClosure = (Error) -> ()
typealias VAEmptySuccessClosure = ()->()

class VARequest<T:Codable> {

    typealias VARequestSuccessClosure = (T) -> ()
    
    var path:String {
        return "/"
    }
    var httpMethod:VAHttpMethod {
        return .GET
    }
    var needsAuthentication:Bool {
        return true
    }
    var dateDecodingStrategy:JSONDecoder.DateDecodingStrategy {
        return .secondsSince1970
    }
    
    private var httpHeaders:[String:String] = [
        "Content-Type":"application/json",
        "Accept":"application/json"
    ]
    
    private var queryItems:[URLQueryItem]?
    private var bodyItems:[String:Any]?
    
    
    var url: URL? {
        var components = URLComponents()
        components.scheme = VAConfigurationManager.shared.config.urlScheme
        components.host = VAConfigurationManager.shared.config.host
        components.path = path
        components.queryItems = queryItems
        return components.url
    }
    
    func addHeader(name:String, value:String) {
        httpHeaders[name] = value
    }
    
    
    func addQueryItem(name:String, value:String?) {
        if queryItems == nil {
            queryItems = []
        }
        queryItems?.append(.init(name: name, value: value))
    }
    
    func addBodyItem(name:String, value:Any) {
        if bodyItems == nil {
            bodyItems = [:]
        }
        bodyItems?[name] = value
    }
    
    func handleError(failClosure: VARequestFailClosure?, error:Error) {
        if VAConfigurationManager.shared.config.debugModeEnabled {
            print(error.localizedDescription)
        }
        failClosure?(error)
    }
    
    private func generateRequest(url:URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = self.httpMethod.rawValue
        self.httpHeaders.forEach({request.setValue($0.value, forHTTPHeaderField: $0.key)})
               
        if let bodyItems = self.bodyItems, let bodyData = try? JSONSerialization.data(withJSONObject: bodyItems, options: []) {
           request.httpBody = bodyData
        }
        return request
    }
    
    func startRequest(_ onSuccess:VARequestSuccessClosure? = nil, _ onFail:VARequestFailClosure? = nil) {
        guard let url = self.url else {
            self.handleError(failClosure: onFail, error: VAError.missingUrl)
            return
        }
        let session = URLSession.shared
        let request = generateRequest(url: url)
        
        let task = session.dataTask(with: request) { (data, response, error) in
          
            if let error = error {
                onFail?(error)
            } else if let data = data {
                
                if VAConfigurationManager.shared.config.debugModeEnabled {
                    if let responseStr = String(data: data, encoding: .utf8) {
                        print(responseStr)
                    }
                }
                
                //On success
                let responseOK = {
                    do {
                      let decoder = JSONDecoder()
                      decoder.dateDecodingStrategy = self.dateDecodingStrategy
                        let response = try decoder.decode(T.self , from: data)
                        onSuccess?(response)

                    } catch {
                        onFail?(error)
                    }
                }
                
                //On fail
                let apiError = {
                    do {
                      let decoder = JSONDecoder()
                      decoder.dateDecodingStrategy = self.dateDecodingStrategy
                        let response = try decoder.decode(VAAPIError.self , from: data)
                        onFail?(response.error)

                    } catch {
                        onFail?(error)
                    }
                }
                
                if let response = response as? HTTPURLResponse {
                    if response.statusCode >= 200 && response.statusCode < 300 {
                        responseOK()
                    }
                    else {
                        apiError()
                    }
                }
                

                
            }
        }
        
        task.resume()
    }
    
    
    func request(_ onSuccess:VARequestSuccessClosure? = nil, _ onFail:VARequestFailClosure? = nil) {
        if needsAuthentication {
            VASessionManager.shared.checkAuthentication(onSuccess: { 
                guard let accessToken = VASessionManager.shared.session?.accessToken else {
                    self.handleError(failClosure: onFail, error: VAError.missingAccessToken)
                    return
                }
                self.addHeader(name: "accessToken", value: accessToken)
                self.startRequest(onSuccess, onFail)
            }, onFail: onFail)
        }else {
            startRequest({ (response) in
                onSuccess?(response)
            }) { (error) in
                if let error = error as? VAError {
                    switch  error {
                    case .AccessTokenExpired,
                         .InvalidAccessToken,
                         .InvalidSession:
                        VASessionManager.shared.authenticate(onSuccess: {
                            self.request(onSuccess, onFail)
                        }, onFail: onFail)
                    default:
                        onFail?(error)
                    }
                }else {
                    onFail?(error)
                }
            }
        }
    }

}
