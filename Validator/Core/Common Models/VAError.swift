//
//  VAError.swift
//  Validator Sample
//
//  Created by Fire on 14.04.2020.
//  Copyright © 2020 MobileX. All rights reserved.
//

import UIKit

enum VAError: Error {
    case missingUrl
    case parseFailed
    case missingAccessToken
    case missingCrendetials
    case missingRefreshToken
    case missingApplicationId
    case AccessTokenExpired
    case InvalidAccessToken
    case InvalidSession
    case UnknownError
}

extension VAError: LocalizedError {
    
    public var errorDescription: String? {
        switch self {
        case .missingUrl:
            return "Missing url"
        case .parseFailed:
            return "Error Decoding Response Model Object"
        case .missingAccessToken:
            return "Missing access token"
        case .missingCrendetials:
            return "Missing crendtials"
        case .missingRefreshToken:
            return "Missing refresh token"
        case .missingApplicationId:
            return "Missing application id"
        case .AccessTokenExpired:
            return "Access token expire"
        case .InvalidAccessToken:
            return "Invalid access token"
        case .UnknownError:
            return "Unknown error"
        case .InvalidSession:
            return "Invalid sesion"
        }
    }
    
}

