//
//  VAConfiguration.swift
//  ValidatorExample
//
//  Created by Orhan DALGARA on 13.04.2020.
//  Copyright © 2020 Orhan DALGARA. All rights reserved.
//

import UIKit
import AdSupport

public enum VAEnvironmnet: String {
    case Sandbox = "Sandbox"
    case Production = "Production"
}

public let VAPlatformToolConfigAdjustKey = "adjust"
public let VAPlatformToolConfigAppsflyerKey = "appsflyer"

public enum VAPlatformToolConfig {
    
    case Adjust(adjustId: String?)
    case Appsflyer(appsflyerId: String?)
    
    var key: String {
        switch self {
        case .Adjust:
            return VAPlatformToolConfigAdjustKey
        case .Appsflyer:
            return VAPlatformToolConfigAppsflyerKey
        }
    }
    
}

extension Array where Element == VAPlatformToolConfig {
    
    func toDictionary() -> [String: String] {
        var dict: [String: String] = [:]
        forEach { v in
            switch v {
            case .Adjust(let adjustId):
                if let id = adjustId, !id.isEmpty {
                    dict[v.key] = id
                }
            case .Appsflyer(let appsflyerId):
                if let id = appsflyerId, !id.isEmpty {
                    dict[v.key] = id
                }
            }
        }
        return dict
    }
    
}

class VAConfiguration {

    var environment: VAEnvironmnet = .Production
    var debugModeEnabled: Bool = false
    var applicationKey: String?
    var applicationSecret: String?
    
    fileprivate var adIdManager: ASIdentifierManager {
        return ASIdentifierManager.shared()
    }
    
    var idfa: String? {
        guard isAdTrackingEnabled else { return nil }
        return adIdManager.advertisingIdentifier.uuidString
    }
    var isAdTrackingEnabled: Bool {
        return adIdManager.isAdvertisingTrackingEnabled
    }
    
    var host: String {
        switch environment {
        case .Sandbox:
            return "api.validatorplus.com"
        case .Production:
            return "api.validatorplus.com"
        }
    }
    
    var urlScheme:String {
        switch environment {
        case .Sandbox:
            return "https"
        case .Production:
            return "https"
        }
    }
    
//    private override init() {
//        super.init()
//    }
//
//    convenience init(appToken:String, platformTools:VAPlatformToolConfig..., environment:VAEnvironmnet = .Production) {
//        self.init()
//        self.environment = environment
//        self.platformTools = platformTools
//    }
//
//    required init(from decoder: Decoder) throws {
//        try super.init(from: decoder)
//    }
    
}
