//
//  VASession.swift
//  Validator Sample
//
//  Created by Fire on 29.04.2020.
//  Copyright © 2020 MobileX. All rights reserved.
//

import UIKit

class VASession: Codable {
    var refreshToken:String?
    var accessToken:String?
    var accessTokenExpiresAt:Date?
    
    private enum CodingKeys: String, CodingKey {
        case refreshToken = "refreshToken"
        case accessToken = "accessToken"
        case accessTokenExpiresAt = "accessTokenExpiresAt"
    }

}
