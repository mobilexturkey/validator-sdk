//
//  Application.swift
//  Validator Sample
//
//  Created by Fire on 29.04.2020.
//  Copyright © 2020 MobileX. All rights reserved.
//

import UIKit

class VAApplication: Codable {
    var id:String?
    var name:String?
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }
}
