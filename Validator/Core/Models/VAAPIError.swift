//
//  VAAPIError.swift
//  Validator
//
//  Created by Fire on 17.05.2020.
//

import UIKit

class VAAPIError: Codable {

    var errCode:Int?
    var errMessage:String?
    var name:String?
    var status:Int?
    
    private enum CodingKeys: String, CodingKey {
        case errCode = "errCode"
        case errMessage = "errMessage"
        case name = "name"
        case status = "status"
    }
    
    var error:VAError {
        guard let errCode = errCode else {return .UnknownError}
        switch errCode {
        case 10003:
            return .AccessTokenExpired
        case 10001:
            return .InvalidAccessToken
        case 10007:
            return .InvalidSession
        default:
            return .UnknownError
        }
    }
    
}
