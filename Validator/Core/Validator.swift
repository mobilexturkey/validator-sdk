//
//  Validator.swift
//  Validator Sample
//
//  Created by Fire on 14.04.2020.
//  Copyright © 2020 MobileX. All rights reserved.
//

import UIKit

@objc public class Validator: NSObject {
    
    @objc public static var debugModeEnabled: Bool {
        set {
            VAConfigurationManager.shared.config.debugModeEnabled = newValue
        }
        get {
            return VAConfigurationManager.shared.config.debugModeEnabled
        }
    }
    
//    public static var environment: VAEnvironmnet {
//        set {
//            VAConfigurationManager.shared.config.environment = newValue
//        }
//        get {
//            return VAConfigurationManager.shared.config.environment
//        }
//    }

    public static func logPurchase(receipt: String,
                                   price: Double,
                                   currency: String,
                                   introductoryPrice: Double? = nil,
                                   platformTools: [VAPlatformToolConfig]) {
        VAValidateReceiptRequest.init(receiptData: receipt,
                                      price: price,
                                      currency: currency,
                                      introductoryPrice: introductoryPrice,
                                      platformTools: platformTools).request()
    }
    
    @objc public static func logPurchase(receipt: String,
                                   price: Double,
                                   currency: String,
                                   introductoryPrice: Double,
                                   platformTools: [String: String]) {
        VAValidateReceiptRequest.init(receiptData: receipt,
                                      price: price,
                                      currency: currency,
                                      introductoryPrice: introductoryPrice,
                                      platformTools: platformTools).request()
    }
    
//    public static func configure(applicationKey:String,
//                          applicationSecret:String,
//                          platformTools:VAPlatformToolConfig...,
//                          environment:VAEnvironmnet = .Production) {
//
//        VAConfigurationManager.shared.config.applicationKey = applicationKey
//        VAConfigurationManager.shared.config.applicationSecret = applicationSecret
//        VAConfigurationManager.shared.config.platformTools = platformTools
//        VAConfigurationManager.shared.config.environment = .Sandbox
//
//    }
    
    @objc public static func configure(applicationKey: String,
                          applicationSecret: String) {
        VAConfigurationManager.shared.config.applicationKey = applicationKey
        VAConfigurationManager.shared.config.applicationSecret = applicationSecret
        VAConfigurationManager.shared.config.environment = .Production
    }
    
}
