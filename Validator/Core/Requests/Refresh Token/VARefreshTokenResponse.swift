//
//  VARefreshTokenResponse.swift
//  Validator Sample
//
//  Created by Fire on 29.04.2020.
//  Copyright © 2020 MobileX. All rights reserved.
//

import UIKit

class VARefreshTokenResponse: Codable {
    var session:VASession?
    private enum CodingKeys: String, CodingKey {
        case session = "session"
    }
}
