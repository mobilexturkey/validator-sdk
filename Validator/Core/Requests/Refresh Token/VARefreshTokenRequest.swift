//
//  VARefreshTokenRequest.swift
//  Validator Sample
//
//  Created by Fire on 29.04.2020.
//  Copyright © 2020 MobileX. All rights reserved.
//
    
import UIKit

class VARefreshTokenRequest: VARequest<VAAuthenticateResponse> {
    
    convenience init(refreshToken:String) {
        self.init()
        addBodyItem(name: "refreshToken", value: refreshToken)
    }
    
    override var path: String {
        return "/api/v1/refreshApplicationToken"
    }
    
    override var httpMethod: VAHttpMethod {
        return .POST
    }
    
    override var needsAuthentication: Bool {
        return false
    }
}
