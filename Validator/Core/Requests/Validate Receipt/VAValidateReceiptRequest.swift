//
//  VAValidateReceiptRequest.swift
//  ValidatorExample
//
//  Created by Orhan DALGARA on 9.04.2020.
//  Copyright © 2020 Orhan DALGARA. All rights reserved.
//

import UIKit

class VAValidateReceiptRequest: VARequest<VAValidateReceiptResponse> {
    
    convenience init(receiptData: String, price: Double, currency: String, introductoryPrice: Double? = nil, platformTools: [VAPlatformToolConfig]) {
        self.init(receiptData: receiptData,
                  price: price,
                  currency: currency,
                  introductoryPrice: introductoryPrice,
                  platformTools: platformTools.toDictionary())
    }
    
    convenience init(receiptData: String, price: Double, currency: String, introductoryPrice: Double? = nil, platformTools: [String: String]) {
        self.init()
        
        var thirdPartyIds = [[String :Any]]()
        
        platformTools.keys.forEach { key in
            //Default values
            var tool: [String: Any] = [
                "key": key,
                "price": price,
                "currencyCode": currency
            ]
            
            if let introductoryPrice = introductoryPrice, introductoryPrice > 0 {
                tool["introductoryPrice"] = introductoryPrice
            }
            
            if let idfa = VAConfigurationManager.shared.config.idfa {
                tool["idfa"] = idfa
            }

            if key == VAPlatformToolConfigAdjustKey, let adjustId = platformTools[key], !adjustId.isEmpty {
                tool["adjustId"] = adjustId
            }
            
            if key == VAPlatformToolConfigAppsflyerKey, let appsflyerId = platformTools[key], !appsflyerId.isEmpty {
                tool["appsflyerId"] = appsflyerId
            }
            
            thirdPartyIds.append(tool)
        }

        addBodyItem(name: "platform", value: "ios")
        addBodyItem(name: "thirdPartyIds", value: thirdPartyIds)
        addBodyItem(name: "receipt", value: receiptData)
        
    }
    
    override var httpMethod: VAHttpMethod {
        return .POST
    }

    override var path: String {
        return "/api/v1/validate"
    }
    

}
