//
//  VAValidateReceiptResponse.swift
//  ValidatorExample
//
//  Created by Orhan DALGARA on 9.04.2020.
//  Copyright © 2020 Orhan DALGARA. All rights reserved.
//

import UIKit

class VAValidateReceiptResponse: Codable {

    var title:String?
    
    private enum CodingKeys: String, CodingKey {
        case title = "title"
    }
    
}
