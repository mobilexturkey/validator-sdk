//
//  VAAuthenticateResponse.swift
//  Validator Sample
//
//  Created by Fire on 29.04.2020.
//  Copyright © 2020 MobileX. All rights reserved.
//

import UIKit

class VAAuthenticateResponse: Codable {
    
    var session:VASession?
    var application:VAApplication?
    private enum CodingKeys: String, CodingKey {
        case session = "sessionDetails"
        case application = "application"
    }
    
}
