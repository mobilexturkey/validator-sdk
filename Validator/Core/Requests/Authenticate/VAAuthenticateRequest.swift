//
//  VAAuthenticateRequest.swift
//  Validator Sample
//
//  Created by Fire on 29.04.2020.
//  Copyright © 2020 MobileX. All rights reserved.
//

import UIKit

class VAAuthenticateRequest: VARequest<VAAuthenticateResponse> {
    convenience init(applicationKey:String, applicationSecret:String) {
        self.init()
        addBodyItem(name: "applicationKey", value: applicationKey)
        addBodyItem(name: "applicationSecret", value: applicationSecret)
    }
    
    override var path: String {
        return "/api/v1/loginApplication"
    }
    
    override var httpMethod: VAHttpMethod {
        return .POST
    }
    
    override var needsAuthentication: Bool {
        return false
    }
}
