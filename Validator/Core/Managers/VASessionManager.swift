//
//  VASessionManager.swift
//  Validator Sample
//
//  Created by Fire on 29.04.2020.
//  Copyright © 2020 MobileX. All rights reserved.
//

import UIKit

class VASessionManager {
    
    static let shared = VASessionManager()
    private init () {}
    private let keychain = Keychain(service: "validator-sdk-keychain")
    
    var session:VASession? {
        set {
            guard let applicationKey = VAConfigurationManager.shared.config.applicationKey else {return}
            if newValue == nil {
                try? keychain.remove(applicationKey)
                return
            }
            guard let encodedSession = try? JSONEncoder().encode(newValue) else {return}
            try? keychain.set(encodedSession, key: applicationKey)
        }
        get {
            guard let applicationKey = VAConfigurationManager.shared.config.applicationKey else {return nil}
            guard let data = try? keychain.getData(applicationKey) else {return nil}
            return try? JSONDecoder().decode(VASession.self, from: data)
        }
    }
    
    private var hasSession:Bool {
        guard let _ = session else {return false}
        return true
    }
    

    
    func checkAuthentication(onSuccess:@escaping VAEmptySuccessClosure, onFail: VARequestFailClosure? = nil) {
        if hasSession {
            updateRefreshTokenIfNeeded(onSuccess: onSuccess, onFail: onFail)
        }else {
            authenticate(onSuccess: onSuccess, onFail: onFail)
        }
    }
    
    func removeSession() {
        session = nil
    }
    
    
    func updateRefreshTokenIfNeeded(onSuccess:@escaping VAEmptySuccessClosure, onFail: VARequestFailClosure? = nil) {
        let refreshDate = session?.accessTokenExpiresAt?.timeIntervalSince1970 ?? 0
        let currentDate = Date().timeIntervalSince1970
        if currentDate > refreshDate {
            guard let refreshToken = session?.refreshToken else {
                onFail?(VAError.missingRefreshToken)
                return
            }
            
            VARefreshTokenRequest.init(refreshToken: refreshToken).request({ (response) in
                self.handleAuthResponse(response: response)
                onSuccess()
            }, onFail)
        }else {
            onSuccess()
        }
    }
    
    func authenticate(onSuccess:@escaping VAEmptySuccessClosure, onFail: VARequestFailClosure? = nil) {
        guard
            let applicationKey = VAConfigurationManager.shared.config.applicationKey,
            let applicationSecret = VAConfigurationManager.shared.config.applicationSecret
        else{
            onFail?(VAError.missingCrendetials)
            return
        }
        
        VAAuthenticateRequest.init(applicationKey: applicationKey, applicationSecret: applicationSecret).request({ (response) in
            self.handleAuthResponse(response: response)
            onSuccess()
        }, nil)
    }
    
    func handleAuthResponse(response:VAAuthenticateResponse) {
        self.session = response.session
    }
    
}
