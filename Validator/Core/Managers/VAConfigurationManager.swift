//
//  VAConfigurationManager.swift
//  ValidatorExample
//
//  Created by Orhan DALGARA on 13.04.2020.
//  Copyright © 2020 Orhan DALGARA. All rights reserved.
//

import UIKit



class VAConfigurationManager {
    
    static let shared = VAConfigurationManager()
    private (set) var config:VAConfiguration
    
    private init() {
        config = VAConfiguration()
    }
    
}
