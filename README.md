# Add the SDK to your project

Add the following line to your Podfile

```
pod 'ValidatorKit'
```

# Integrate the SDK into your app

If you added the Validator SDK via a Pod repository, you should use one of the following import statements:

```
import ValidatorKit
```

In the Project Navigator, open the source file of your application delegate. Add the import statement at the top of the file, then add the following call to Validator in the `didFinishLaunching` or `didFinishLaunchingWithOptions` method of your app delegate:

```
 let applicationKey = "{YourApplicationToken}"
 let applicationSecret = "{YourAplicationSecreet}"
 Validator.configure(applicationKey: applicationKey, applicationSecret: applicationSecret)

```
Replace `{YourApplicationToken}` and `{YourAplicationSecreet}` with yours. You can find this in your dashboard.  

# Supported Networks

Validator SDK can track attribution from the following networks:

- **[Adjust SDK]**
- **More SDKs soon**

[Adjust SDK]: <https://github.com/adjust/ios_sdk> 

# Install the Adjust SDK

You will need to integrate the **[Adjust SDK]** into your app before you can connect the data to Validator.

# Tracking attribution

In order to verify in-app purchases, you need to call the `Validator.logPurchase` method on the `Validator` instance. Please make sure to call this method after `finishTransaction` in `func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction])` only if the state changed to `SKPaymentTransactionStatePurchased`

```
  public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
    for transaction in transactions {
      switch (transaction.transactionState) {
      case .purchased:
       
        SKPaymentQueue.default().finishTransaction(transaction)
        
        if  let receiptURL = Bundle.main.appStoreReceiptURL,
            let receiptData = try? Data.init(contentsOf: receiptURL) {
            let receiptBase64 = receiptData.base64EncodedString()
            Validator.logPurchase(receipt: receiptBase64, price: price, currency: currency,  platformTools: [.Adjust(adjustId:Adjust.adid())])
        }
        break
      case .failed:
        // Your stuff if any.
        break
      case .restored:
        // Your stuff if any.
        break
      case .deferred:
        // Your stuff if any.
        break
      case .purchasing:
        // Your stuff if any.
        break
      }
    }
  }
```

Log purchase method exects you to pass following parameters:

````
receipt         : App receipt of Data type
price           : Local price of current SKProduct object
currency        : Local currency of current SKProduct object
platformTools.  : Needed platform tools to track attributions via Validator.
````
