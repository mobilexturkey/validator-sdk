//
//  AppDelegate.swift
//  Validator Sample
//
//  Created by H. Eren ÇELİK on 9.04.2020.
//  Copyright © 2020 MobileX. All rights reserved.
//

import UIKit
import ValidatorKit
import Adjust
import Firebase
import AppsFlyerLib

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    
    func configure() {
        let appToken = "tzhsdxawgv7k"
        #if DEBUG
        let envrionment = ADJEnvironmentSandbox
        #else
        let envrionment = ADJEnvironmentProduction
        #endif
        guard let config = ADJConfig(appToken: appToken, environment: envrionment) else { return }
        Adjust.appDidLaunch(config)
    }
    
    func configureAppsflyer() {
        
        AppsFlyerTracker.shared().appsFlyerDevKey = "nSQNqBpM4byyd9GoY2nVgf"
        AppsFlyerTracker.shared().appleAppID = "1507757167"
        #if DEBUG
        AppsFlyerTracker.shared().isDebug = true
        #endif

    }
    
    @objc func sendLaunch(app:Any) {
        AppsFlyerTracker.shared().trackAppLaunch()
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        configure()
        FirebaseApp.configure()
        configureAppsflyer()
        Validator.configure(applicationKey: "eafd907545a1a1f9b2da4359abf39a76", applicationSecret: "0a6cd8583618e4767c96eebbe705d5b9")
        Validator.debugModeEnabled = true
        
        NotificationCenter.default.addObserver(self,
               selector: #selector(sendLaunch),
               // For Swift version < 4.2 replace name argument with the commented out code
               name: UIApplication.didBecomeActiveNotification, //.UIApplicationDidBecomeActive for Swift < 4.2
               object: nil)

        return true
            
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

