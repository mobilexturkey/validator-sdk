//
//  ViewController.swift
//  Validator
//
//  Created by Fire on 30.04.2020.
//  Copyright © 2020 Fire. All rights reserved.
//

import UIKit
import StoreKit
import ValidatorKit
import AppsFlyerLib

class ViewController: UIViewController {

    private var store: IAPHelper!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        store = Products.store
    }
    
    @IBAction func purchase1MonthWithIntroductoryTapped(_ sender: Any) {
        guard let product = store.product(forIdentifier: Products.Month1WithIntroductory) else { return }
        store.buyProduct(product)
    }
    
    @IBAction func purchase1MonthTapped(_ sender: Any) {
        guard let product = store.product(forIdentifier: Products.Month1) else { return }
        store.buyProduct(product)
    }
    
    @IBAction func trialTapped(_ sender: Any) {
        guard let product = store.product(forIdentifier: Products.Month1WithTrial) else { return }
        store.buyProduct(product)
    }

}

