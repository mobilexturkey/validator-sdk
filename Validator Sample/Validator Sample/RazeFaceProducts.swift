
import Foundation

public struct Products {
  
    public static let Month1WithIntroductory = "com.validator.eo.1MonthIntroductory"
    public static let Month1 = "com.valitator.eo.1Month"
    public static let Month1WithTrial = "com.validator.eo.1MonthWithTrial"

    private static let productIdentifiers: Set<ProductIdentifier> = [
        Products.Month1,
        Products.Month1WithTrial,
        Products.Month1WithIntroductory
    ]

    public static let store = IAPHelper(productIds: Products.productIdentifiers)

}
